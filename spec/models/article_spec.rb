require "rails_helper"
describe Article do

  describe "#create" do
    before do
      @title = "Test Title"
      @text = "This is a lot of example text, some would say too much even"
      @no_title_article = Article.new ({:title => nil, :text => @text})
      @no_text_article = Article.new ({:title => @title, :text => nil})
      @empty_param_article = Article.new ({})
      @valid_article = Article.new ({:title => @title, :text => @text})

      @articles = [
          @no_title_article,
          @no_text_article,
          @empty_param_article,
          @valid_article
      ]
    end

    describe "given no title" do
      it "fails to save" do
        expect(@no_title_article.save).to be false
      end
    end

    describe "given no text" do
      it "fails to save" do
        expect(@no_text_article.save).to be false
      end
    end

    describe "given no title and no text" do
      it "fails to save" do
        expect(@empty_param_article.save).to be false
      end
    end

    describe "given a valid title and text" do
      it "saves successfully" do
        expect(@valid_article.save).to be true
      end
    end

  end
end