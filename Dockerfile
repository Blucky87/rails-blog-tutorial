

FROM ruby:2.5

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

EXPOSE 3000

ARG STAGE_DIR
ARG PROJ_DIR

RUN mkdir -p ${STAGE_DIR} && \
    mkdir -p ${PROJ_DIR}

ADD Gemfile ${STAGE_DIR}/Gemfile
ADD Gemfile.lock ${STAGE_DIR}/Gemfile.lock

WORKDIR ${STAGE_DIR}
RUN bundle install




