#!/usr/bin/env bash
# The output of all these installation steps is noisy. With this utility
# the progress report is nice and concise.


function install {
    echo "Installing ${1}..."
    shift
    apt-get -y install "${@}" >/dev/null 2>&1
}

echo "Adding Swap File"
fallocate -l 1G /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo '/swapfile none swap defaults 0 0' >> /etc/fstab

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

echo "deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main" >> /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -



echo "Updating Package Information"
apt-get -y update >/dev/null 2>&1

install 'Depends' git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common  ruby-all-dev libpq-dev libffi-dev nodejs yarn ruby-dev postgresql-10

# sudo -u postgres createuser -s -d -P vagrant
sudo -u postgres psql -c "CREATE USER vagrant WITH PASSWORD 'vagrant';"
sudo -u postgres psql -c "ALTER USER vagrant WITH SUPERUSER;"

# sudo -u postgres psql <<END_OF_SQL
# CREATE DATABASE default;
# CREATE SCHEMA test;
# CREATE USER vagrant WITH ENCRYPTED PASSWORD 'vagrant';
# GRANT ALL PRIVILEGES ON DATABASE default TO vagrant;
# GRANT ALL ON SCHEMA test TO vagrant;
# END_OF_SQL

install 'Ruby' ruby

# echo installing current RubyGems
gem update --system -N >/dev/null 2>&1

sudo gem install rails

gem install rake

# sudo gem install ruby-debug-ide  --source 'https://rubygems.org/'

sudo gem install debase

echo "Installing Bundler"
gem install bundler -N >/dev/null 2>&1

/usr/bin/ruby /vagrant/bin/bundle install

# Needed for docs generation.
update-locale LANG=en_US.UTF-8 LANGUAGE=en_US.UTF-8 LC_ALL=en_US.UTF-8

echo "cd /vagrant" >> /home/vagrant/.bash_rc

echo 'all set, rock on!'
